type Currency = 'EUR';
type StringBoolean = '0' | '1';

type Size = {
  width: number;
  height: number;
};

type Coords = {
  x: number;
  y: number;
};

interface Position {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}

interface ElementContent {
  id: string;
  title: string;
}
