import * as React from 'react';

import { hot, setConfig } from 'react-hot-loader';

import { injectGlobal, ThemeProvider } from 'styled-components';

import ComponentRoot from './components/ComponentRoot';
import SvgRoot from './components/SvgRoot';
import TargetElement from './components/elements/TargetElement';

import FamilyTreeStructure from './structures/FamilyTree';
import TargetElementStructure from './structures/TargetElement';
import RoleElementStructure from './structures/Roles/RoleElement';
import ItemElementStructure from './structures/Items/ItemElement';
import ItemToRoleArrowElementStructure from './structures/Arrows/ItemToRoleArrowElement';
import RoleToTargetArrowElementStructure from './structures/Arrows/RoleToTargetArrowElement';

import RoleElement from './components/elements/RoleElement';
import ItemElement from './components/elements/ItemElement';

import Arrow from './components/arrows/Arrow';
import ArrowTrack from './components/arrows/ArrowTrack';
import ArrowEventHandlersLayer from './components/arrows/ArrowEventHandlersLayer';

import {
  MEMBER_ROLE,
  GOVERNMENT_ROLE,
  CONVOCATION_ROLE,
  LIQUIDATOR_ROLE,
  PROCURATOR_ROLE
} from './constants/roles';

import {
  PRIVATE_PERSON,
  LEGAL_ENTITY
} from './constants/types';

import { isNull } from 'util';
import { RoleElementProps } from './interfaces/elements';

import * as en from 'react-intl/locale-data/en';
import * as ru from 'react-intl/locale-data/ru';
import * as lv from 'react-intl/locale-data/lv';
import { addLocaleData, IntlProvider } from 'react-intl';

import messages from './messages';
import Range from './components/Range';
import { ProcessingItemElemContent, RoleElemContent, TargetElemContent } from './interfaces/elemsContent';

addLocaleData([
  ...en,
  ...ru,
  ...lv
]);

setConfig({ logLevel: 'debug' });

// tslint:disable-next-line
injectGlobal`
  #temp_elems {
    height: 0;
    width: 0;
    overflow: hidden;
  }
`;

interface Theme {
  roles: {
    [Role: string]: string;
  };
  types: {
    [Type: string]: string;
  };
}

let theme: Theme;

if (process.env.NODE_ENV !== 'production') {
  // tslint:disable-next-line
  injectGlobal`
    * { margin: 0; padding: 0; }  
  `;

  theme = {
    roles: {
      [MEMBER_ROLE]: 'rgb(0, 109, 157)',
      [GOVERNMENT_ROLE]: 'rgb(175, 90, 142)',
      [CONVOCATION_ROLE]: 'rgb(128, 157, 91)',
      [LIQUIDATOR_ROLE]: 'rgb(220, 77, 77)',
      [PROCURATOR_ROLE]: 'rgb(92, 73, 126)'
    },
    types: {
      [PRIVATE_PERSON]: 'rgb(170, 214, 243)',
      [LEGAL_ENTITY]: 'rgb(182, 218, 149)'
    }
  };
} else {
  // tslint:disable-next-line
  theme = window['theme'];
}

let FT_JSON_URL: string;

if (process.env.NODE_ENV !== 'production') {
  FT_JSON_URL = '/mocks/store.json';
} else {
  FT_JSON_URL = '';
}

interface ResponseResult {
  lang: string;
  scheme: {
    target: TargetElemContent;
    parents: Array<ProcessingItemElemContent>;
    children: Array<ProcessingItemElemContent>;
  };
  roles: Array<RoleElemContent>;
}

interface Props {
  width: number;
  height: number;
}

type State = {
  scaleCoeff: number;
  lang: string;
  targetElement: TargetElementStructure | null;
  roleElements: Array<RoleElementStructure>;
  itemElements: Array<ItemElementStructure>;
  itemToRoleArrowElements: Array<Array<ItemToRoleArrowElementStructure>>;
  roleToTargetArrowElements: Array<RoleToTargetArrowElementStructure>;
  targetElementCentralPosition: Coords;
  width: number;
  height: number;
  error?: object;
};

export const ThemeContext: React.Context<Theme> = React.createContext(theme);

class App extends React.Component<Props, State> {
  state: State;

  constructor(props: Props) {
    super(props);

    this.state = {
      scaleCoeff: 1,
      lang: 'lv',
      targetElement: null,
      roleElements: [],
      itemElements: [],
      itemToRoleArrowElements: [],
      roleToTargetArrowElements: [],
      targetElementCentralPosition: {x: 0, y: 0},
      height: 0,
      width: 0
    };

    this.onRangeInputChange = this.onRangeInputChange.bind(this);
  }

  async componentDidMount() {
    const response: Response = await fetch(FT_JSON_URL);
    const result: ResponseResult = await response.json();

    const {
      lang,
      scheme: {
        target,
        parents,
        children
      },
      roles
    } = result;

    const familyTreeObject: FamilyTreeStructure = new FamilyTreeStructure(
      target,
      parents,
      children,
      roles
    );

    this.setState({
      lang,
      targetElement: familyTreeObject!.targetElement,
      roleElements: [
        ...familyTreeObject.parentGroup.roleElements,
        ...familyTreeObject.childGroup.roleElements
      ],
      itemElements: [
        ...familyTreeObject.parentGroup.itemElements,
        ...familyTreeObject.childGroup.itemElements
      ],
      itemToRoleArrowElements: [
        ...familyTreeObject.parentGroup.itemToRoleArrowsGroup.arrowsByRoles,
        ...familyTreeObject.childGroup.itemToRoleArrowsGroup.arrowsByRoles
      ],
      roleToTargetArrowElements: [
        ...familyTreeObject.parentGroup.roleToTargetArrowsGroup.arrowElements,
        ...familyTreeObject.childGroup.roleToTargetArrowsGroup.arrowElements
      ],
      targetElementCentralPosition: familyTreeObject.targetElementCentralPosition,
      width: familyTreeObject.width,
      height: familyTreeObject.height
    });
  }

  protected onRangeInputChange(e: React.ChangeEvent<HTMLInputElement>): void {
    e.preventDefault();
    e.persist();

    // tslint:disable-next-line
    console.log(e.target.value);

    this.setState((): Pick<State, 'scaleCoeff'> => {
      return {
        scaleCoeff: parseFloat(e.target.value)
      };
    });
  }

  render() {
    const {
      scaleCoeff,
      lang,
      targetElement,
      roleElements,
      itemElements,
      itemToRoleArrowElements,
      roleToTargetArrowElements,
      targetElementCentralPosition,
      width: innerWidth,
      height: innerHeight,
      error
    } = this.state;

    const {
      width: outerWidth,
      height: outerHeight
    } = this.props;

    if (isNull(targetElement) || (targetElement!.id === '')) {
      return (
        <div>Loading...</div>
      );
    }

    if (error !== undefined) {
      return (
        <div>Network error!!!</div>
      );
    }

    const maxRangeValue: number = Math.max(
      innerHeight / outerHeight,
      innerWidth / outerWidth
    );

    return (
      <IntlProvider
        locale={lang}
        messages={messages[lang]}
      >
        <ThemeProvider theme={theme}>
          <ComponentRoot
            width={outerWidth}
            height={outerHeight}
          >
            <Range
              maxRangeValue={maxRangeValue}
              scaleCoeff={scaleCoeff}
              onChange={this.onRangeInputChange}
            />
            <SvgRoot
              width={innerWidth}
              height={innerHeight}
              outerWidth={outerWidth}
              outerHeight={outerHeight}
              targetElementCentralPositionX={targetElementCentralPosition.x}
              targetElementCentralPositionY={targetElementCentralPosition.y}
              scaleCoeff={scaleCoeff}
            >
              {(itemToRoleArrowElements.length > 0) ?
                itemToRoleArrowElements
                  .map((
                    arrowElements: Array<ItemToRoleArrowElementStructure>,
                    groupIndex: number
                  ): JSX.Element => (
                    <React.Fragment key={groupIndex}>
                      {arrowElements
                        .map(({
                                d
                              }: ItemToRoleArrowElementStructure,
                              index: number
                        ): JSX.Element => (
                          <ArrowTrack
                            d={d}
                            key={index}
                          />
                        ))}
                      {arrowElements
                        .map(({
                                d,
                                role
                              }: ItemToRoleArrowElementStructure,
                              index: number
                        ): JSX.Element => (
                          <Arrow
                            d={d}
                            role={role}
                            key={index}
                          />
                        ))}
                      {arrowElements
                        .map(({
                                d,
                                tooltipInfo
                              }: ItemToRoleArrowElementStructure,
                              index: number
                        ): JSX.Element => (
                          <ArrowEventHandlersLayer
                            d={d}
                            tooltipInfo={tooltipInfo}
                            key={index}
                          />
                        ))}
                    </React.Fragment>
                  )) : null}
              {(roleToTargetArrowElements.length > 0) ?
                roleToTargetArrowElements
                  .map(({
                          d,
                          role
                        }: RoleToTargetArrowElementStructure,
                        index: number
                  ): JSX.Element => (
                    <Arrow
                      d={d}
                      role={role}
                      key={index}
                    />
                  )) : null}
              {
                (roleElements.length > 0) ?
                  roleElements.map(({
                                      title,
                                      role,
                                      centralCoords,
                                      size
                                    }: RoleElementStructure,
                                    index: number
                  ): React.ReactElement<RoleElementProps> => (
                    <RoleElement
                      title={title}
                      role={role}
                      size={size}
                      centralCoords={centralCoords}
                      key={index}
                    />
                  )) : null
              }
              {
                (itemElements.length > 0) ?
                  itemElements.reverse().map(({
                                                id,
                                                title,
                                                roles,
                                                roleArr,
                                                type,
                                                tooltipInfo,
                                                centralCoords,
                                                size
                                              }: ItemElementStructure,
                                              index: number
                  ): React.ReactElement<ItemElementStructure> => (
                    <ItemElement
                      id={id}
                      title={title}
                      tooltipInfo={tooltipInfo}
                      roles={roles}
                      roleArr={roleArr}
                      type={type}
                      centralCoords={centralCoords}
                      size={size}
                      key={index}
                    />
                  )) : null
              }
              <TargetElement
                id={targetElement.id}
                title={targetElement.title}
                type={targetElement.type}
                tooltipInfo={targetElement.tooltipInfo}
                size={targetElement.size}
                centralCoords={targetElement.centralCoords}
              />
            </SvgRoot>
          </ComponentRoot>
        </ThemeProvider>
      </IntlProvider>
    );
  }
}

export default hot(module)(App);
