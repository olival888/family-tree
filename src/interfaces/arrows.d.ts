import * as React from 'react';

export interface ArrowProps {
  role: string;
  d: string;
  className?: string;
}

export interface ArrowTrackProps {
  d: string;
  className?: string;
}

export interface ArrowEventHandlersLayerProps {
  onMouseEnter?: (event: React.MouseEvent<SVGPathElement>) => void;
  onMouseLeave?: () => void;
  d: string;
  tooltipInfo: Partial<ArrowTooltitInfo>;
  className?: string;
}

export interface ArrowTooltitInfo {
  startDate: string;
  partCur: string;
  partSum: string;
  percent: string;
  holding: string;
  position: string;
}
