import Element from '../structures/Element';
import RoleElement from '../structures/Roles/RoleElement';
import ItemElement from '../structures/Items/ItemElement';
import * as React from 'react';

export interface WrapperProps extends Pick<ItemElement, 'size' | 'centralCoords'> {
  onMouseEnter?: (event: React.MouseEvent<SVGGElement>) => void;
  onMouseLeave?: (event: React.MouseEvent<SVGGElement>) => void;
  className?: string;

  children: Array<JSX.Element | null> | JSX.Element;
}

export interface TargetElementProps extends Element {
  className?: string;
}

export interface ItemElementProps extends ItemElement {
  className?: string;
}

export interface RoleElementProps extends RoleElement {
  className?: string;
}
