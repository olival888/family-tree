export interface TooltipInfo {
  legalForm: string;
  address: string;
  regDate: string;
  capCur: Currency;
  capSum: string;
  capFullyPaid: StringBoolean;
}

export interface ElemInfo {
  id: string;
  regNo: string;
  title: string;

  favorites: StringBoolean;
  url_s: string;
  type: string;
}

export interface RoleInfo {
  startDate: string;
  holding: StringBoolean;
  partCur?: Currency;
  partSum?: string;
  percent?: string;
  position?: string;
}

export interface ElemContent extends Partial<TooltipInfo>, ElemInfo {}

export interface TargetElemContent extends ElemContent {}

export interface ItemElemContent extends ElemContent {
  roles: {
    [key: string]: RoleInfo
  };
}

export interface ProcessingElemContent extends ElemInfo {
  tooltipInfo: Partial<TooltipInfo>;
}

export interface ProcessingTargetElemContent extends ProcessingElemContent {}

export interface ProcessingItemElemContent extends ElemContent {
  tooltipInfo: Partial<TooltipInfo>;
  roleArr: Array<string>;
  roles: {
    [key: string]: RoleInfo
  };
}

export interface RoleElemContent {
  role: string;
  title: string;
}
