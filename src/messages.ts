export default {
  'en': {
    legalForm: 'Legal form',
    address: 'Address',
    regDate: 'Registration date',
    shareCapital: 'Share capital',
    fullyPaid: 'fully paid'
  },
  'lv': {
    legalForm: 'Tiesiskā forma',
    address: 'Adrese',
    regDate: 'Reģistrācijas datums',
    shareCapital: 'Pamatkapitāls',
    fullyPaid: 'pilnībā apmaksāts'
  },
  'ru': {
    legalForm: 'Правовая форма',
    address: 'Адрес',
    regDate: 'Дата регистрации',
    shareCapital: 'Уставный капитал',
    fullyPaid: 'полностью оплачен'
  }
};