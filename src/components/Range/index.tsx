import * as React from 'react';

import styled from 'styled-components';

interface RangeProps {
  maxRangeValue: number;
  scaleCoeff: number;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  className?: string;
}

const Range: React.SFC<RangeProps> = ({
  maxRangeValue,
  scaleCoeff,
  onChange,
  className
}: RangeProps): JSX.Element => (
  <input
    type="range"
    min={1}
    max={maxRangeValue}
    step={(maxRangeValue - 1) / 100}
    value={scaleCoeff}
    onChange={onChange}
    className={className}
  />
);

export default styled(Range)`
  width: 150px;
  height: 20px;
  transform-origin: 75px 75px;
  transform: rotate(-270deg);
  position: absolute;
  top: 20px;
  left: -115px;
  z-index: 1;
`;