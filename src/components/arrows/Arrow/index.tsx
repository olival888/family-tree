import * as React from 'react';

import { ArrowProps } from '../../../interfaces/arrows';
import styled from 'styled-components';

const Arrow: React.SFC<ArrowProps> = ({
                                        d,
                                        role,
                                        className
                                      }: ArrowProps): JSX.Element => (
  <path
    role={role}
    d={`${d} l 3 -5l-3 5l-3 -5`}
    className={className}
  />
);

interface StyledProps {
  theme?: {
    roles: object;
    types: object;
  };
  role?: string;
}

export default styled(Arrow)`
  stroke: ${({role, theme}: StyledProps): string => 
    role ? theme!.roles![role] : ''};
  fill: none;
  stroke-width: 2;
`;
