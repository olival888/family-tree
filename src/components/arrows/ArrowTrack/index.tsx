import * as React from 'react';

import { ArrowTrackProps } from '../../../interfaces/arrows';
import styled from 'styled-components';

const ArrowTrack: React.SFC<ArrowTrackProps> = ({
                                                  d,
                                                  className
}: ArrowTrackProps) => (
  <path
    d={d}
    className={className}
  />
);

export default styled(ArrowTrack)`
  stroke: #fff;
  fill: none;
  stroke-width: 6;
`;
