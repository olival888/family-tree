// ToDo: HOC
import * as React from 'react';

import ArrowBubble from '../../bubbles/ArrowBubble';
import { ArrowEventHandlersLayerProps } from '../../../interfaces/arrows';

interface State {
  hover: boolean;
  cursorCoord: Coords;
}

class ArrowEventHandlersLayer extends React.Component<ArrowEventHandlersLayerProps, State> {
  state: State = {
    hover: false,
    cursorCoord: {
      x: 0, y: 0
    }
  };

  constructor(props: ArrowEventHandlersLayerProps) {
    super(props);

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  protected onMouseEnter(event: React.MouseEvent<SVGPathElement>): void {
    this.setState({
      hover: true,
      cursorCoord: {
        x: event.clientX,
        y: event.clientY
      }
    });
  }

  protected onMouseLeave(): void {
    this.setState({
      hover: false,
      cursorCoord: {
        x: 0, y: 0
      }
    });
  }

  render() {
    const {
      d,
      // tooltipInfo,
      className
    }: ArrowEventHandlersLayerProps = this.props;

    const {
      hover,
      cursorCoord
    }: State = this.state;

    return (
      <>
        <path
          d={d}
          stroke="#fff"
          strokeOpacity={0.00001}
          strokeWidth={8}
          fill="none"
          onMouseEnter={this.onMouseEnter}
          onMouseLeave={this.onMouseLeave}
          className={className}
        />
        {(hover) ?
          <ArrowBubble
            cursorCoord={cursorCoord}
          >
            <div />
          </ArrowBubble> :
          null
        }
      </>
    );
  }
}
/*FormattedMessage*/
export default ArrowEventHandlersLayer;
