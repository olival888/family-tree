import * as React from 'react';
import { RootContext } from '../ComponentRoot';

// const INNER_SVG_PADDING = 60;

interface SvgRootProps {
  scaleCoeff: number;
  width: number;
  height: number;
  outerWidth: number;
  outerHeight: number;
  targetElementCentralPositionX: number;
  targetElementCentralPositionY: number;
  className?: string;
}
interface State {
  moveable: boolean;
  offset: {
    top: number;
    left: number;
  };
  position: {
    top: number;
    left: number;
  };
}

class SvgRoot extends React.Component<SvgRootProps, State> {
  divRef: React.RefObject<HTMLDivElement>;
  state: State;

  constructor(props: SvgRootProps) {
    super(props);

    const {
      outerWidth,
      outerHeight,
      width: innerWidth,
      height: innerHeight,
      targetElementCentralPositionX,
      targetElementCentralPositionY
    } = props;

    const width: number = Math.max(innerWidth, outerWidth);
    const height: number = Math.max(innerHeight, outerHeight);

    this.state = {
      moveable: false,
      offset: {
        top: 0,
        left: 0
      },
      position: {
        left: outerWidth / 2 - (targetElementCentralPositionX + (width - innerWidth) / 2),
        top: outerHeight / 2 - (targetElementCentralPositionY + (height - innerHeight) / 2)
      }
    };

    this.onDragStartHandler = this.onDragStartHandler.bind(this);
    this.onDragHandler = this.onDragHandler.bind(this);
    this.onDragEnd = this.onDragEnd.bind(this);

    this.divRef = React.createRef<HTMLDivElement>();
  }

  protected onDragStartHandler(
    rootRef: HTMLDivElement
  ): React.EventHandler<React.MouseEvent<HTMLDivElement>> {
    const rootRect: ClientRect = rootRef.getBoundingClientRect();

    return (
      event: React.MouseEvent<HTMLDivElement>,
      topPosition: number = rootRect.top,
      leftPosition: number = rootRect.left
    ): void => {
      const divElement: HTMLDivElement = this.divRef.current as HTMLDivElement;

      const { top, left }: ClientRect = divElement.getBoundingClientRect();

      this.setState({
        moveable: true,
        offset: {
          top: event.clientY - top + topPosition,
          left: event.clientX - left + leftPosition
        }
      });
    };
  }

  protected onDragHandler(
    rootRef: HTMLDivElement
  ): React.EventHandler<React.MouseEvent<HTMLDivElement>> {
    const {
      scaleCoeff
    }: SvgRootProps = this.props;

    const rootRect: ClientRect = rootRef.getBoundingClientRect();

    return (
      event: React.MouseEvent<HTMLDivElement>,
      rootRectWidth: number = rootRect.width,
      rootRectHeight: number = rootRect.height,
      scaleCoeffArg: number = scaleCoeff
    ): void => {
      const divElement: HTMLDivElement = this.divRef.current as HTMLDivElement;
      const {
        width: divRectWidth,
        height: divRectHeight
      }: ClientRect = divElement.getBoundingClientRect();

      if (this.state.moveable) {
        event.preventDefault();
        event.persist();

        this.setState((state: State, props: SvgRootProps): Pick<State, 'position'> => {
          if (!state.offset) {
            return {
              position: state.position
            };
          }

          let topSvgIndent: number;
          let leftSvgIndent: number;

          // ToDo: ScaleCoef

          if ((event.clientY - state.offset.top) > 0) {
            topSvgIndent = 0;
          } else if ((event.clientY - state.offset.top) < (rootRectHeight - divRectHeight)) {
            topSvgIndent = rootRectHeight - divRectHeight;
          } else {
            topSvgIndent = event.clientY - state.offset.top;
          }

          if ((event.clientX - state.offset.left) > 0) {
            leftSvgIndent = 0;
          } else if ((event.clientX - state.offset.left) < (rootRectWidth - divRectWidth)) {
            leftSvgIndent = rootRectWidth - divRectWidth;
          } else {
            leftSvgIndent = event.clientX - state.offset.left;
          }

          return {
            position: {
              top: topSvgIndent,
              left: leftSvgIndent
            }
          };
        });
      }
    };
  }

  protected onDragEnd(e: React.MouseEvent<HTMLDivElement>): void {
    e.preventDefault();

    this.setState({
      moveable: false,
      offset: {
        top: 0,
        left: 0
      }
    });
  }

  public render(): JSX.Element {
    const {
      scaleCoeff,
      width: innerWidth,
      height: innerHeight,
      outerWidth,
      outerHeight,
      targetElementCentralPositionX,
      targetElementCentralPositionY,
      className,
      children
    } = this.props;

    const {
      moveable,
      position
    } = this.state;

    const width: number = Math.max(innerWidth, outerWidth);
    const height: number = Math.max(innerHeight, outerHeight);

    return (
      <RootContext.Consumer>
        {(rootRef: HTMLDivElement): JSX.Element => (
          <div
            ref={this.divRef}
            style={{
              position: 'absolute',
              top: position.top,
              left: position.left,
              cursor: moveable ? 'grabbing' : 'grab'
            }}
            onMouseDown={this.onDragStartHandler(rootRef)}
            onMouseMove={this.onDragHandler(rootRef)}
            onMouseUp={this.onDragEnd}
            onMouseLeave={this.onDragEnd}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width={width}
              height={height}
              className={className}
              viewBox={`-${(targetElementCentralPositionX + (width - innerWidth) / 2) * scaleCoeff} ` +
              `-${(targetElementCentralPositionY + (height - innerHeight) / 2) * scaleCoeff} ` +
              `${width * scaleCoeff} ${height * scaleCoeff}`}
              preserveAspectRatio="none"
              style={{
                // border: '2px solid #000'
              }}
            >
              {children}
            </svg>
          </div>
        )}
      </RootContext.Consumer>

    );
  }
}

export default SvgRoot;