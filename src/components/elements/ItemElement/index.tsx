import * as React from 'react';

import Element from '../Element';

import { ItemElementProps } from '../../../interfaces/elements';
import { ELEMENT_BLOCK_PADDING, ELEMENT_FONT_SIZE } from '../../../constants/commonElementsStyleProps';
import { ThemeContext } from '../../../App';

import ElementBubble from '../../bubbles/ElementBubble';

interface State {
  hover: boolean;
}

class ItemElement extends React.Component<ItemElementProps, State> {
  state: State = {
    hover: false,
  };

  itemRef: React.RefObject<SVGGElement | null> = React.createRef();

  constructor(props: ItemElementProps) {
    super(props);

    this.onMouseEnter = this.onMouseEnter.bind(this);
    this.onMouseLeave = this.onMouseLeave.bind(this);
  }

  protected onMouseEnter(): void {
    this.setState({
      hover: true,
    });
  }

  protected onMouseLeave(): void {
    this.setState({
      hover: false
    });
  }

  public render(): JSX.Element {
    const {
      id,
      title,
      type,
      tooltipInfo,
      size,
      centralCoords,
      className
    } = this.props;

    const {
      hover
    } = this.state;

    const x: number = centralCoords.x - size.width / 2;
    const y: number = centralCoords.y - size.height / 2;

    return (
      <ThemeContext.Consumer>
        {(theme): JSX.Element => (
          <Element
            className={className}
            size={size}
            centralCoords={centralCoords}
            onMouseEnter={this.onMouseEnter}
            onMouseLeave={this.onMouseLeave}
            ref={this.itemRef}
          >
            <rect
              width={size.width}
              height={size.height}
              x={x}
              y={y}
              fill={theme.types[type]}
            />
            <text
              x={x}
              y={y + ELEMENT_FONT_SIZE}
              dx={ELEMENT_BLOCK_PADDING}
              dy={ELEMENT_BLOCK_PADDING}
              fontWeight="bold"
              strokeWidth={0}
            >
              {id}
            </text>
            <text
              x={x}
              y={y + size.height}
              dx={ELEMENT_BLOCK_PADDING}
              dy={-ELEMENT_BLOCK_PADDING - 3}
              strokeWidth={0}
            >
              {title}
            </text>

            {
              ((type === '1') && hover) ?
                <ElementBubble
                  tooltipInfo={tooltipInfo}
                  itemRef={this.itemRef.current as SVGGElement}
                /> :
                null
            }
          </Element>
        )}
      </ThemeContext.Consumer>
    );
  }
}

export default ItemElement;
