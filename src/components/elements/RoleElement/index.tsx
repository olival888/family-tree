import * as React from 'react';

import Element from '../Element/index';

import { RoleElementProps } from '../../../interfaces/elements';

import styled from 'styled-components';

import { ThemeContext } from '../../../App';
import { ELEMENT_BLOCK_PADDING, ELEMENT_FONT_SIZE } from '../../../constants/commonElementsStyleProps';

const RoleElement: React.SFC<RoleElementProps> = ({
  title,
  role,
  size,
  centralCoords,
  className
}: RoleElementProps) => {
  const x: number = centralCoords.x - size.width / 2;
  const y: number = centralCoords.y - size.height / 2;

  return (
    <ThemeContext.Consumer>
      {(theme): JSX.Element => (
        <Element
          size={size}
          centralCoords={centralCoords}
          className={className}
        >
          <rect
            width={size.width}
            height={size.height}
            x={x}
            y={y}
            fill={theme.roles[role]}
          />
          <text
            x={x}
            y={y + ELEMENT_FONT_SIZE}
            dx={ELEMENT_BLOCK_PADDING}
            dy={size.height / 2 - ELEMENT_FONT_SIZE / 2 - 3}
          >
            {title}
          </text>
        </Element>
      )}
    </ThemeContext.Consumer>
  );
};

export default styled(RoleElement)`
  fill: #fff;
`;