import * as React from 'react';

import { WrapperProps } from '../../../interfaces/elements';

//
import { ELEMENT_FONT_SIZE } from '../../../constants/commonElementsStyleProps';
import { ClassAttributes } from 'react';

const Element: React.ComponentType<WrapperProps & ClassAttributes<SVGGElement>> =
  React.forwardRef<SVGGElement, WrapperProps>(({
                                                size: {
                                                  width,
                                                  height
                                                },
                                                centralCoords: {
                                                  x: centralX,
                                                  y: centralY
                                                },
                                                onMouseEnter,
                                                onMouseLeave,
                                                children,
                                                className
                                              }: WrapperProps,
                                               ref: React.RefObject<SVGGElement>
): JSX.Element => (
  <g
    width={width}
    height={height}
    x={centralX - width / 2}
    y={centralY - height / 2}
    onMouseEnter={onMouseEnter}
    onMouseLeave={onMouseLeave}
    ref={ref}

    style={{
      // fill: '#000',
      fontFamily: 'Arial',
      fontSize: ELEMENT_FONT_SIZE,
      // strokeWidth: 0
    }}
    className={className}
  >
    {children}
  </g>
));

export default Element;

/*
export default styled(Element)`

`;*/
