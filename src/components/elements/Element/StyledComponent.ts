import styled from 'styled-components';
import { ELEMENT_FONT_SIZE } from '../../../constants/commonElementsStyleProps';

export default styled('g')`
  fill: #000; 
  font-family: 'Arial'; 
  font-size: ${ELEMENT_FONT_SIZE}px; 
  stroke-width: 0;
`;