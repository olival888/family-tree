import * as React from 'react';

import ItemElement from '../ItemElement';

import styled from 'styled-components';
import { TargetElementProps } from '../../../interfaces/elements';

const TargetElement: React.SFC<TargetElementProps> = ({
  id,
  title,
  type,
  tooltipInfo,
  size,
  centralCoords,
  className
}: TargetElementProps) => (
  <ItemElement
    id={id}
    title={title}
    type={type}
    tooltipInfo={tooltipInfo}
    size={size}
    centralCoords={centralCoords}
    roles={{}}
    roleArr={[]}
    className={className}
  />
);

export default styled(TargetElement)`
  stroke-width: 5px;
  stroke: rgb(248, 184, 76);
`;
