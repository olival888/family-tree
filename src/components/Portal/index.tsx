import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { isNull } from 'util';

interface Props {
  top: number;
  left: number;
  portalRoot: HTMLDivElement | null;
  children: JSX.Element;
}

class Portal extends React.Component<Props> {
  el: HTMLDivElement;
  constructor(props: Props) {
    super(props);
    this.el = document.createElement('div');
    this.el.setAttribute(
      'style',
      `position: absolute;top: ${props.top}px;left: ${props.left}px`
    );
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.top !== this.props.top || prevProps.left !== this.props.left) {
      this.el.setAttribute(
        'style',
        `position: absolute;top: ${this.props.top}px;left: ${this.props.left}px`
      );
    }
  }

  componentDidMount() {
    if (!isNull(this.props.portalRoot)) {
      this.props.portalRoot.appendChild(this.el);
    }
  }

  componentWillUnmount() {
    if (!isNull(this.props.portalRoot)) {
      this.props.portalRoot.removeChild(this.el);
    }
  }

  render() {
    return ReactDOM.createPortal(
      this.props.children,
      this.el,
    );
  }
}

export default Portal;