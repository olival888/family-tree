export const RIGHT_LEFT_PADDING: number = 35;
export const TOP_BOTTOM_PADDING: number = 30;

export const FILL_COLOR: string = 'rgb(255, 252, 195)';
export const STROKE_COLOR: string = 'rgb(108, 108, 108)';