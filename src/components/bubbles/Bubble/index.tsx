import * as React from 'react';

import Portal from '../../Portal';
import { RootContext } from '../../ComponentRoot';

import { ClassAttributes } from 'react';
import { CSSOffset, CSSPosition, RelativePosition } from '../types';
import {
  FILL_COLOR,
  RIGHT_LEFT_PADDING,
  STROKE_COLOR,
  TOP_BOTTOM_PADDING
} from '../constants';

interface Props {
  width: number;
  height: number;
  position: RelativePosition;
  getBase: (
    position: RelativePosition,
    topBottomPadding: number,
    leftRightPadding: number
  ) => CSSPosition;
  children: Array<JSX.Element | string | null> | JSX.Element | string | null;
}

const Bubble: React.ComponentType<Props & ClassAttributes<HTMLDivElement>> =
  React.forwardRef<HTMLDivElement, Props>(({
                                             width,
                                             height,
                                             position,
                                             getBase,
                                             children
                                           }: Props,
                                           ref: React.RefObject<HTMLDivElement>
  ): JSX.Element => {
    const padding: CSSOffset = calcPadding(position);
    const d: string = calcD(position, width, height, TOP_BOTTOM_PADDING, RIGHT_LEFT_PADDING);
    const base: CSSPosition = getBase(position, TOP_BOTTOM_PADDING, RIGHT_LEFT_PADDING);

    return (
      <RootContext.Consumer>
        {(componentRootRef: HTMLDivElement) => (
          <Portal
            top={base.top}
            left={base.left}
            portalRoot={componentRootRef}
          >
            <div
              style={{
                width,
                backgroundImage: `url("data:image/svg+xml;charset=UTF-8,` +
                `<svg xmlns='http://www.w3.org/2000/svg' ` +
                  `viewBox='0 0 ${width + RIGHT_LEFT_PADDING + 1} ${height + TOP_BOTTOM_PADDING + 1}'>` +
                  `<path d='${d}' fill='${FILL_COLOR}' stroke-width='1' stroke='${STROKE_COLOR}' />` +
                `</svg>")`,
                backgroundRepeat: 'no-repeat',
                paddingTop: padding.top,
                paddingLeft: padding.left,
                paddingRight: padding.right,
                paddingBottom: padding.bottom
              }}
            >
              <div
                style={{
                  fontFamily: 'Arial',
                  fontSize: 12,
                  padding: 10,
                  boxSizing: 'border-box',
                  lineHeight: 1.5
                }}
                ref={ref}
              >
                {children}
              </div>
            </div>
          </Portal>
        )}
      </RootContext.Consumer>
    );
  });

function calcPadding(position: RelativePosition): CSSOffset {
  switch (position) {
    case 'top left':
      return {
        top: undefined,
        bottom: TOP_BOTTOM_PADDING,
        left: undefined,
        right: RIGHT_LEFT_PADDING
      };
    case 'top right':
      return {
        top: undefined,
        bottom: TOP_BOTTOM_PADDING,
        left: RIGHT_LEFT_PADDING,
        right: undefined
      };
    case 'bottom left':
      return {
        top: TOP_BOTTOM_PADDING,
        bottom: undefined,
        left: undefined,
        right: RIGHT_LEFT_PADDING
      };
    case 'bottom right':
      return {
        top: TOP_BOTTOM_PADDING,
        bottom: undefined,
        left: RIGHT_LEFT_PADDING,
        right: undefined
      };
    default:
      return {};
  }
}

function calcD(
  position: RelativePosition,
  width: number,
  height: number,

  topBottomPadding: number,
  leftRightPadding: number
): string {
  switch (position) {
    case 'top left':
      return `M${width + leftRightPadding} ${height + topBottomPadding} `
        + `l${-(leftRightPadding + 5)} ${-topBottomPadding}`
        + `h ${-(width - 5)}`
        + `v ${-height}`
        + `h ${width}`
        + `v ${height - 5}`
        + `z`;
    case 'top right':
      return `M0 ${height + topBottomPadding} `
        + `l${leftRightPadding + 5} ${-topBottomPadding}`
        + `h ${width - 5}`
        + `v ${-height}`
        + `h ${-(width)}`
        + `v ${(height - 5)}`
        + `z`;
    case 'bottom left':
      return `M${width + leftRightPadding} 0 `
        + `l${-(leftRightPadding + 5)} ${topBottomPadding}`
        + `h ${-(width - 5)}`
        + `v ${height}`
        + `h ${width}`
        + `v ${-height + 5}`
        + `z`;
    case 'bottom right':
      return `M0 0 `
        + `l${leftRightPadding + 5} ${topBottomPadding}`
        + `h ${width - 5}`
        + `v ${height}`
        + `h ${-(width)}`
        + `v ${-(height - 5)}`
        + `z`;
    default:
      return '';
  }
}

export default Bubble;