import * as React from 'react';

import Bubble from '../Bubble';

import { RootContext } from '../../ComponentRoot';

import { CSSPosition, RelativePosition } from '../types';
import { isNull } from 'util';

interface Props {
  cursorCoord: Coords;
  children: JSX.Element;
}

interface State {
  height: number;
}

class ArrowBubble extends React.Component<Props, State> {
  bubbleRef: React.RefObject<HTMLDivElement | null> = React.createRef<HTMLDivElement>();

  state: State = {
    height: 0
  };

  componentDidMount() {
    const {
      height
    }: ClientRect = (this.bubbleRef.current as HTMLDivElement).getBoundingClientRect();

    this.setState({
      height: height
    });
  }

  protected getPosition(
    bubbleRef: HTMLDivElement | null,
    componentRootRef: HTMLDivElement,
    cursorCoord: Coords
  ): RelativePosition {
    if (isNull(bubbleRef)) {
      return undefined;
    }

    const bubbleRect: ClientRect = bubbleRef.getBoundingClientRect();
    const rootRect: ClientRect = componentRootRef.getBoundingClientRect();

    const {
      // top: bubbleRefTop,
      // left: bubbleRefLeft,
      width: bubbleRefWidth,
      height: bubbleRefHeight
    }: ClientRect = bubbleRect;

    const {
      top: componentRootRefTop,
      left: componentRootRefLeft,
      width: componentRootRefWidth,
      // height: componentRootRefHeight
    }: ClientRect = rootRect;

    function getVerticalPosition(
      componentRootRefTopArg: number,
      bubbleRefHeightArg: number,
      cursorCoordY: number
    ): 'top' | 'bottom' {
      if ((cursorCoordY - componentRootRefTopArg) > bubbleRefHeightArg) {
        return 'top';
      }

      return 'bottom';
    }

    function getHorisontalPosition(
      componentRootRefLeftArg: number,
      cursorCoordX: number,
      componentRootRefWidthArg: number,
      bubbleRefWidthArg: number
    ): 'left' | 'right' {
      if (
        (componentRootRefWidthArg - cursorCoordX - componentRootRefLeftArg) > bubbleRefWidthArg
      ) {
        return 'right';
      }

      return 'left';
    }

    const verticalPosition: string = getVerticalPosition(
      componentRootRefTop,
      cursorCoord.y,
      bubbleRefHeight
    );

    const horisontalPosition: string = getHorisontalPosition(
      componentRootRefLeft,
      cursorCoord.x,
      componentRootRefWidth,
      bubbleRefWidth
    );

    return `${verticalPosition} ${horisontalPosition}` as RelativePosition;
  }

  protected getBase(
    bubbleRef: HTMLDivElement | null,
    componentRootRef: HTMLDivElement,
    cursorCoord: Coords
  ): (
    position: RelativePosition,
    topBottomPadding: number,
    leftRightPadding: number
  ) => CSSPosition {
    if (isNull(bubbleRef)) {
      return () => ({top: 0, left: 0});
    }

    const {
      width: bubbleRectWidth,
      height: bubbleRectHeight
    }: ClientRect = bubbleRef.getBoundingClientRect();

    const {
      top: componentRootRectTop,
      left: componentRootRectLeft
    }: ClientRect = componentRootRef.getBoundingClientRect();

    const itemPositionTop: number = cursorCoord.y - componentRootRectTop;
    const itemPositionLeft: number = cursorCoord.x - componentRootRectLeft;

    return (
      position: RelativePosition,

      topBottomPadding: number,
      leftRightPadding: number
    ) => {
      switch (position) {
        case 'top left':
          return {
            top: itemPositionTop - bubbleRectHeight - topBottomPadding,
            left: itemPositionLeft - bubbleRectWidth - leftRightPadding
          };
        case 'top right':
          return {
            top: itemPositionTop - bubbleRectHeight - topBottomPadding,
            left: itemPositionLeft
          };
        case 'bottom left':
          return {
            top: itemPositionTop,
            left: itemPositionLeft - bubbleRectWidth - leftRightPadding
          };
        case 'bottom right':
          return {
            top: itemPositionTop,
            left: itemPositionLeft
          };
        default:
          return {
            top: 0,
            left: 0
          };
      }
    };
  }

  render() {
    const {
      cursorCoord,
      children
    } = this.props;

    const {
      height = 0
    } = this.state;

    return (
      <RootContext.Consumer>
        {(componentRoot: HTMLDivElement | null) => (
          <Bubble
            getBase={this.getBase(
              this.bubbleRef.current as HTMLDivElement,
              componentRoot as HTMLDivElement,
              cursorCoord
            )}
            position={this.getPosition(
              this.bubbleRef.current as HTMLDivElement,
              componentRoot as HTMLDivElement,
              cursorCoord
            )}
            width={200}
            height={height}
            ref={this.bubbleRef}
          >
            {children}
          </Bubble>
        )}
      </RootContext.Consumer>
    );
  }
}

export default ArrowBubble;