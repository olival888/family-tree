import * as React from 'react';

import Bubble from '../Bubble';
import { FormattedMessage } from 'react-intl';
import { RootContext } from '../../ComponentRoot';

import { CSSPosition, RelativePosition } from '../types';
import { isNull } from 'util';
import { TooltipInfo } from '../../../interfaces/elemsContent';

interface Props {
  itemRef: SVGGElement;
  tooltipInfo: Partial<TooltipInfo>;
}

interface State {
  height: number;
}

class ElementBubble extends React.Component<Props, State> {
  bubbleRef: React.RefObject<HTMLDivElement | null> = React.createRef<HTMLDivElement>();

  state: State = {
    height: 0
  };

  componentDidMount() {
    const {
      height
    }: ClientRect = (this.bubbleRef.current as HTMLDivElement).getBoundingClientRect();

    this.setState({
      height: height
    });
  }

  protected getPosition(
    bubbleRef: HTMLDivElement | null,
    itemRef: SVGGElement,
    componentRootRef: HTMLDivElement
  ): RelativePosition {
    if (isNull(bubbleRef)) {
      return undefined;
    }

    const bubbleRect: ClientRect = bubbleRef.getBoundingClientRect();
    const itemRect: ClientRect = itemRef.getBoundingClientRect();
    const rootRect: ClientRect = componentRootRef.getBoundingClientRect();

    const {
      // top: bubbleRefTop,
      // left: bubbleRefLeft,
      width: bubbleRefWidth,
      height: bubbleRefHeight
    }: ClientRect = bubbleRect;

    const {
      top: componentRootRefTop,
      left: componentRootRefLeft,
      width: componentRootRefWidth,
      // height: componentRootRefHeight
    }: ClientRect = rootRect;

    const {
      top: itemRefTop,
      left: itemRefLeft,
      width: itemRefWidth,
      // height: itemRefHeight
    }: ClientRect = itemRect;

    function getVerticalPosition(
      componentRootRefTopArg: number,
      itemRectTopArg: number,
      bubbleRefHeightArg: number
    ): 'top' | 'bottom' {
      if ((itemRectTopArg - componentRootRefTopArg) > (bubbleRefHeightArg + 20 + 10)) {
        return 'top';
      }

      return 'bottom';
    }

    function getHorisontalPosition(
      componentRootRefLeftArg: number,
      itemRectLeftArg: number,
      componentRootRefWidthArg: number,
      itemRectWidthArg: number,
      bubbleRefWidthArg: number
    ): 'left' | 'right' {
      if (
        ((componentRootRefWidthArg + componentRootRefLeftArg) - (itemRectLeftArg + itemRectWidthArg))
          > (bubbleRefWidthArg + 20 + 10)
      ) {
        return 'right';
      }

      return 'left';
    }

    const verticalPosition: string = getVerticalPosition(
      componentRootRefTop,
      itemRefTop,
      bubbleRefHeight
    );

    const horisontalPosition: string = getHorisontalPosition(
      componentRootRefLeft,
      itemRefLeft,
      componentRootRefWidth,
      itemRefWidth,
      bubbleRefWidth
    );

    return `${verticalPosition} ${horisontalPosition}` as RelativePosition;
  }

  protected getBase(
    bubbleRef: HTMLDivElement | null,
    itemRef: SVGGElement,
    componentRootRef: HTMLDivElement,
    coverPadding: number
  ): (
    position: RelativePosition,
    topBottomPadding: number,
    leftRightPadding: number
  ) => CSSPosition {
    if (isNull(bubbleRef)) {
      return () => ({top: 0, left: 0});
    }

    const {
      width: bubbleRectWidth,
      height: bubbleRectHeight
    }: ClientRect = bubbleRef.getBoundingClientRect();

    const {
      width: itemRectWidth,
      height: itemRectHeight,
      top: itemRectTop,
      left: itemRectLeft
    }: ClientRect = itemRef.getBoundingClientRect();

    const {
      top: componentRootRectTop,
      left: componentRootRectLeft
    }: ClientRect = componentRootRef.getBoundingClientRect();

    const itemPositionTop: number = itemRectTop - componentRootRectTop;
    const itemPositionLeft: number = itemRectLeft - componentRootRectLeft;

    return (
      position: RelativePosition,

      topBottomPadding: number,
      leftRightPadding: number
    ) => {
      switch (position) {
        case 'top left':
          return {
            top: itemPositionTop - bubbleRectHeight - topBottomPadding + coverPadding,
            left: itemPositionLeft - bubbleRectWidth - leftRightPadding + coverPadding
          };
        case 'top right':
          return {
            top: itemPositionTop - bubbleRectHeight - topBottomPadding + coverPadding,
            left: itemPositionLeft + itemRectWidth - coverPadding
          };
        case 'bottom left':
          return {
            top: itemPositionTop + itemRectHeight - coverPadding,
            left: itemPositionLeft - bubbleRectWidth - leftRightPadding + coverPadding
          };
        case 'bottom right':
          return {
            top: itemPositionTop + itemRectHeight - coverPadding,
            left: itemPositionLeft + itemRectWidth - coverPadding
          };
        default:
          return {
            top: 0,
            left: 0
          };
      }
    };
  }

  render() {
    const {
      itemRef,
      tooltipInfo: {
        legalForm,
        address,
        regDate,
        capSum,
        capCur,
        capFullyPaid
      }
    } = this.props;

    const {
      height = 0
    } = this.state;

    return (
      <RootContext.Consumer>
        {(componentRoot: HTMLDivElement | null) => (
          <Bubble
            getBase={this.getBase(
              this.bubbleRef.current as HTMLDivElement,
              itemRef,
              componentRoot as HTMLDivElement,
              10
            )}
            position={this.getPosition(
              this.bubbleRef.current as HTMLDivElement,
              itemRef,
              componentRoot as HTMLDivElement
            )}
            width={250}
            height={height}
            ref={this.bubbleRef}
          >
            {legalForm ?
              <><FormattedMessage id="legalForm" />:&nbsp;{legalForm}<br /></> :
              null}
            {address ?
              <><FormattedMessage id="address" />:&nbsp;{address}<br /></> :
              null}
            {regDate ?
              <><FormattedMessage id="regDate" />:&nbsp;{regDate}<br /></> :
              null}
            {(capSum && capCur && capFullyPaid) ?
              <>
                <FormattedMessage id="shareCapital" />:&nbsp;
                {capCur}&nbsp;
                {capSum},&nbsp;
                {(capFullyPaid === '1') ? <FormattedMessage id="fullyPaid" /> : null}<br />
              </> :
              null}
          </Bubble>
        )}
      </RootContext.Consumer>
    );
  }
}

export default ElementBubble;