export type RelativePosition = 'top left' | 'top right' | 'bottom left' | 'bottom right' | undefined;

export interface CSSOffset {
  top?: number;
  bottom?: number;
  left?: number;
  right?: number;
}

export interface CSSPosition {
  top: number;
  left: number;
}