import * as React from 'react';

export const RootContext: React.Context<HTMLDivElement | null> = React.createContext(null);

interface Props {
  width: number;
  height: number;
}

type State = {
  rootRef: HTMLDivElement | null;
};

class ComponentRoot extends React.Component<Props, State> {
  rootRef: React.RefObject<HTMLDivElement>;

  constructor(props: Props) {
    super(props);

    this.rootRef = React.createRef<HTMLDivElement>();

    this.state = {
      rootRef: null
    };
  }

  componentDidMount() {
    this.setState({
      rootRef: this.rootRef.current
    });
  }

  render() {
    const {
      width,
      height,
      children
    } = this.props;

    const {
      rootRef
    } = this.state;

    return (
      <RootContext.Provider
        value={rootRef}
      >
        <div
          style={{
            border: '1px solid #000',
            width,
            height,
            position: 'relative',
            overflow: 'hidden',
            margin: '100px'
          }}
          ref={this.rootRef}
        >
          {rootRef ? children : null}
        </div>
      </RootContext.Provider>
    );
  }
}

export default ComponentRoot;