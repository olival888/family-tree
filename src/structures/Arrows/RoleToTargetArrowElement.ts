import RoleElement from '../Roles/RoleElement';
import { CHILD, PARENT } from '../../constants/relations';
import {
  INDENTS_BETWEEN_ELEMENTS
} from '../../constants/indents';

import Element from '../TargetElement';

export default
class RoleToTargetArrowElement {
  public d: string = '';
  public role: string;

  constructor(
    roleElement: RoleElement,
    targetElement: Element,
    roleArr: Array<string>,
    relationship: string
  ) {
    const sign: number = (relationship === CHILD) ? 1 : -1;

    const halfRoleElementHeight: number = roleElement.size.height / 2;
    const halfTargetElementHeight: number = targetElement.size.height / 2;

    const roleIndex: number = roleArr.indexOf(roleElement.role);

    const indentFromTargetCenter = (roleIndex * (INDENTS_BETWEEN_ELEMENTS / 2))
      - (Math.floor(roleArr.length / 2) * (INDENTS_BETWEEN_ELEMENTS / 2));

    const indentFromRoleElement: number = Math.abs(
      (roleIndex) * (INDENTS_BETWEEN_ELEMENTS / 2)
        - (Math.floor(roleArr.length / 2) * (INDENTS_BETWEEN_ELEMENTS / 2))
    );

    const rolePoint = {
      x: roleElement.centralCoords.x,
      y: roleElement.centralCoords.y - sign * halfRoleElementHeight
    };

    const targetPoint = {
      x: targetElement.centralCoords.x,
      y: targetElement.centralCoords.y + sign * halfTargetElementHeight
    };

    if (relationship === PARENT) {
      this.d = `M${rolePoint.x} ${rolePoint.y}`
        + `V ${rolePoint.y + indentFromRoleElement}`
        + `H ${targetPoint.x + indentFromTargetCenter}`
        + `V ${targetPoint.y - 3}`;
    } else if (relationship === CHILD) {
      this.d = `M${targetPoint.x + indentFromTargetCenter} ${targetPoint.y - 3}`
        + `V ${rolePoint.y - indentFromRoleElement}`
        + `H ${rolePoint.x}`
        + `V ${rolePoint.y}`;
    }

    this.role = roleElement.role;
  }
}
