import ItemElement from '../Items/ItemElement';
import RoleElement from '../Roles/RoleElement';
import { CHILD, PARENT } from '../../constants/relations';
import {
  INDENTS_BETWEEN_ELEMENTS,
  INDENTS_BETWEEN_LEVELS
} from '../../constants/indents';

import { ELEMENTS_IN_LEVEL } from '../../constants/other';
import { ArrowTooltitInfo } from '../../interfaces/arrows';

export default
class ItemToRoleArrowElement {
  public d: string = '';
  public role: string;
  public percent?: string;
  public tooltipInfo: Partial<ArrowTooltitInfo>;

  constructor(
    itemElement: ItemElement,
    roleElement: RoleElement,
    uniqueRolesInLevel: Array<string>,
    uniqueRolesInItemsGroup: Array<string>,
    indexInLevel: number,
    relationship: string
  ) {
    const sign: number = (relationship === CHILD) ? 1 : -1;

    const halfItemElementHeight = itemElement.size.height / 2;
    const halfRoleElementHeight = roleElement.size.height / 2;

    // tslint:disable-next-line
    console.log(itemElement);

    const horizontalIndentFromElementCenter: number = this._calcHorizontalIndentFromElementCenter(
      itemElement,
      roleElement,
      indexInLevel
    );
    const verticalIndentFromElement: number = this._calcVerticalIndentFromElement(
      itemElement,
      roleElement,
      uniqueRolesInLevel,
    );
    const indent: number = this._calcIndentBetweenElements(
      itemElement,
      roleElement,
      uniqueRolesInItemsGroup
    );

    const elementPoint: Coords = {
      x: itemElement.centralCoords.x
        - horizontalIndentFromElementCenter,
      y: itemElement.centralCoords.y
        - sign * halfItemElementHeight
    };

    const elementsLevelToItemsGroupMiddlelPoint: Coords = {
      x: 0 - indent,
      y: itemElement.centralCoords.y
        - sign * halfItemElementHeight
        - sign * verticalIndentFromElement
    };
    const itemsGroupMiddleToRoleGroupPoint: Coords = {
      x: 0 - indent,
      y: roleElement.centralCoords.y
        + sign * halfRoleElementHeight
        + sign * Math.abs(indent)
    };

    const roleElementPoint: Coords = {
      x: roleElement.centralCoords.x,
      y: roleElement.centralCoords.y
        + sign * halfRoleElementHeight
    };

    if (relationship === PARENT) {
      this.d = `M${elementPoint.x} ${elementPoint.y}`
        + `V ${elementsLevelToItemsGroupMiddlelPoint.y}`
        + `H ${elementsLevelToItemsGroupMiddlelPoint.x}`
        + `V ${itemsGroupMiddleToRoleGroupPoint.y}`
        + `H ${roleElementPoint.x}`
        + `V ${roleElementPoint.y}`
      ;
    } else if (relationship === CHILD) {
      this.d = `M${roleElementPoint.x} ${roleElementPoint.y}`
        + `V ${itemsGroupMiddleToRoleGroupPoint.y}`
        + `H ${itemsGroupMiddleToRoleGroupPoint.x}`
        + `V ${elementsLevelToItemsGroupMiddlelPoint.y}`
        + `H ${elementPoint.x}`
        + `V ${elementPoint.y}`;
    }

    this.role = roleElement.role;

    const {
      startDate,
      partCur,
      partSum,
      percent,
      holding,
      position
    }: Partial<ArrowTooltitInfo> = itemElement.roles[roleElement.role];

    this.percent = percent;

    // role info from itemElement
    this.tooltipInfo = {
      startDate,
      partCur,
      partSum,
      percent,
      holding,
      position
    };
  }

  protected _calcHorizontalIndentFromElementCenter(
    itemElement: ItemElement,
    roleElement: RoleElement,
    indexInLevel: number
  ) {
    const role: string = roleElement.role;
    const roleIndexInElement: number = itemElement.roleArr.indexOf(role);
    const itemRoleArrLength: number = itemElement.roleArr.length;

    if (indexInLevel < ELEMENTS_IN_LEVEL / 2) {
      return (itemRoleArrLength - roleIndexInElement - Math.ceil(itemRoleArrLength / 2))
        * INDENTS_BETWEEN_ELEMENTS / 2
        - ((itemRoleArrLength % 2 === 0) ?
            INDENTS_BETWEEN_ELEMENTS / 2 / 2 :
            0);
    } else {
      return (roleIndexInElement - Math.floor(itemRoleArrLength / 2))
        * INDENTS_BETWEEN_ELEMENTS / 2
        + ((itemRoleArrLength % 2 === 0) ?
            INDENTS_BETWEEN_ELEMENTS / 2 / 2 :
            0);
    }
  }

  protected _calcVerticalIndentFromElement(
    itemElement: ItemElement,
    roleElement: RoleElement,
    uniqueRolesInLevel: Array<string>
  ) {
    const role: string = roleElement.role;
    const roleIndexInLevel: number = uniqueRolesInLevel.indexOf(role);

    return (uniqueRolesInLevel.length - roleIndexInLevel) * INDENTS_BETWEEN_LEVELS / 2;
  }

  protected _calcIndentBetweenElements(
    itemElement: ItemElement,
    roleElement: RoleElement,
    uniqueRolesInItemsGroup: Array<string>
  ) {
    const roleIndexInItemsGroup: number = uniqueRolesInItemsGroup.indexOf(roleElement.role);
    const roleArrLength = uniqueRolesInItemsGroup.length;

    if (roleArrLength % 2 === 0) {
      // ToDo: nepravilno schitautsja rasstojanija pri chetnom chisle rolej
      return (roleArrLength - roleIndexInItemsGroup - roleArrLength / 2)
        * INDENTS_BETWEEN_ELEMENTS / 2;
    }

    return (roleArrLength - roleIndexInItemsGroup - Math.ceil(roleArrLength / 2))
      * INDENTS_BETWEEN_ELEMENTS / 2;
  }
}
