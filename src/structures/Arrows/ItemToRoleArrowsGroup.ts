import ItemElement from '../Items/ItemElement';
import RoleElement from '../Roles/RoleElement';

import SeparatingItems from '../libs/SeparatingItems';
import UniqueItems from '../libs/UniqueItems';

import ItemToRoleArrowElement from './ItemToRoleArrowElement';

import { ELEMENTS_IN_LEVEL } from '../../constants/other';

export default
class ItemToRoleArrowsGroup {
  public arrowsByRoles: Array<Array<ItemToRoleArrowElement>>;

  constructor(
    itemElement: Array<ItemElement>,
    roleElements: Array<RoleElement>,
    relationship: string
  ) {

    const arrowElements: Array<ItemToRoleArrowElement> = this._generateArrows(
      itemElement,
      roleElements,
      relationship
    );

    this.arrowsByRoles = this._groupArrowsByRoles(
      arrowElements,
      roleElements
    );
  }

  protected _generateArrows(
    itemElements: Array<ItemElement>,
    roleElements: Array<RoleElement>,
    relationship: string
  ): Array<ItemToRoleArrowElement> {
    const uniqRolesInItemsGroup: Array<string> = roleElements
      .map(({role}: RoleElement): string => role);
    const separateItemElements: Array<Array<Array<ItemElement>>> =
      SeparatingItems.separationOfElementsByRoles(
        itemElements,
        uniqRolesInItemsGroup
      )
        .map((roleGroup: Array<ItemElement>): Array<Array<ItemElement>> => {
          return SeparatingItems.separationOfElementsByLevels<ItemElement>(roleGroup, ELEMENTS_IN_LEVEL);
        });

    return separateItemElements.map((
      itemsInRoleGroup: Array<Array<ItemElement>>,
      roleIndex: number
    ): Array<ItemToRoleArrowElement> => {
      return itemsInRoleGroup.map((
        itemsInLevel: Array<ItemElement>,
        levelIndex: number
      ): Array<ItemToRoleArrowElement> => {
        const uniqRolesInLevelArr: Array<string> = UniqueItems.getUniqRolesInLevelArr(itemsInLevel);

        return itemsInLevel.map((
          itemElement: ItemElement,
          indexInLevel: number
        ): Array<ItemToRoleArrowElement> => {
          return itemElement.roleArr.map((role: string): ItemToRoleArrowElement => {
            const curRoleElement: RoleElement = roleElements
              .find((roleElement: RoleElement): boolean => (
                roleElement.role === role
              )) as RoleElement;

            return new ItemToRoleArrowElement(
              itemElement,
              curRoleElement,
              uniqRolesInLevelArr,
              uniqRolesInItemsGroup,
              indexInLevel,
              relationship
            );
          });
        }).reduce((
          accumulator: Array<ItemToRoleArrowElement>,
          roleArr: Array<ItemToRoleArrowElement>
        ): Array<ItemToRoleArrowElement> => ([
          ...accumulator,
          ...roleArr
        ]),       []);
      }).reduce((
        accumulator: Array<ItemToRoleArrowElement>,
        roleArr: Array<ItemToRoleArrowElement>
      ): Array<ItemToRoleArrowElement> => ([
        ...accumulator,
        ...roleArr
      ]),       []);
    }).reduce((
      accumulator: Array<ItemToRoleArrowElement>,
      roleArr: Array<ItemToRoleArrowElement>
    ): Array<ItemToRoleArrowElement> => ([
      ...accumulator,
      ...roleArr
    ]),       []);
  }

  protected _groupArrowsByRoles(
    arrowElements: Array<ItemToRoleArrowElement>,
    roleElements: Array<RoleElement>
  ): Array<Array<ItemToRoleArrowElement>> {
    const rolesObj: {[roleName: string]: number} = roleElements.reduce(
      (
        obj: {[roleName: string]: number},
        roleElement: RoleElement,
        i: number
      ): {[roleName: string]: number} => ({
        ...obj,
        [roleElement.role]: i
      })
    , {}
    );

    return arrowElements.reduce(
      (
        result: Array<Array<ItemToRoleArrowElement>>,
        arrow: ItemToRoleArrowElement
      ): Array<Array<ItemToRoleArrowElement>> => {
        const index: number = rolesObj[arrow.role];

        const roleArray: Array<ItemToRoleArrowElement> = [
          ...(result[index] || []),
          arrow
        ];

        return [
          ...result.slice(0, index),
          roleArray,
          ...result.slice(index + 1)
        ];
      }
    , []
    ).reverse();
  }
}
