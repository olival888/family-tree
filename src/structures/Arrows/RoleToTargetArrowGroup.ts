import RoleElement from '../Roles/RoleElement';
import Element from '../TargetElement';

import RoleToTargetArrowElement from './RoleToTargetArrowElement';

export default
class RoleToTargetArrowGroup {
  public arrowElements: Array<RoleToTargetArrowElement>;

  constructor(
    roleElements: Array<RoleElement>,
    targetElement: Element,
    relationship: string
  ) {
    this.arrowElements = this._generateArrows(
      roleElements,
      targetElement,
      relationship
    );
  }

  protected _generateArrows(
    roleElements: Array<RoleElement>,
    targetElement: Element,
    relationship: string
  ): Array<RoleToTargetArrowElement> {
    const roleArr = roleElements
      .map(({role}: RoleElement): string => role);

    return roleElements.map((roleElement: RoleElement): RoleToTargetArrowElement => (
      new RoleToTargetArrowElement(
        roleElement,
        targetElement,
        roleArr,
        relationship
      )
    ));
  }
}
