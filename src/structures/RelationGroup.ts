import RoleElementsGroup from './Roles/RoleElementsGroup';
import ItemsGroup from './Items/ItemsGroup';
import Element from './TargetElement';

import { INDENTS_BETWEEN_ELEMENTS } from '../constants/indents';
import ItemsLevelGroup from './Items/ItemsLevelGroup';
import ItemsRoleGroup from './Items/ItemsRoleGroup';
import RoleElement from './Roles/RoleElement';
import ItemElement from './Items/ItemElement';
import ItemToRoleArrowsGroup from './Arrows/ItemToRoleArrowsGroup';
import RoleToTargetArrowGroup from './Arrows/RoleToTargetArrowGroup';

import UniqueItems from './libs/UniqueItems';
import { ItemElemContent, ProcessingItemElemContent, RoleElemContent } from '../interfaces/elemsContent';
import ProcessingElements from './libs/ProcessingElements';

export default
class RelationGroup {
  public rolesElementsGroup: RoleElementsGroup;
  public itemsGroup: ItemsGroup;

  public itemToRoleArrowsGroup: ItemToRoleArrowsGroup;
  public roleToTargetArrowsGroup: RoleToTargetArrowGroup;

  public roleElements: Array<RoleElement> = [];
  public itemElements: Array<ItemElement> = [];

  public relationship: string;

  protected _targetElement: Element;

  constructor(
    relationship: string,
    elements: Array<ItemElemContent>,
    roles: Array<RoleElemContent>,
    targetElement: Element
  ) {
    this.relationship = relationship;
    this._targetElement = targetElement;

    const processingElements: Array<ProcessingItemElemContent> = elements.map(
      (element: ProcessingItemElemContent): ProcessingItemElemContent =>
        ProcessingElements.processingItemElements(element)
    );

    const rolesArray: Array<string> = UniqueItems.getUniqueRolesInElementArr(processingElements);

    const rolesElements: Array<RoleElemContent> = this._generateRelationElementsForGroup(
      rolesArray,
      roles
    );

    this.rolesElementsGroup = new RoleElementsGroup(
      rolesElements,
      relationship,
      INDENTS_BETWEEN_ELEMENTS
    );

    this.itemsGroup = new ItemsGroup(
      processingElements,
      rolesArray,
      relationship
    );
  }

  public get topYPosition(): number {
    return this.itemsGroup.topYPosition;
  }

  public get maxLeftXPosition(): number {
    return Math.max(
      this.itemsGroup.maxLeftXPosition,
      this.rolesElementsGroup.leftXPosition
    );
  }

  public get maxRightXPosition(): number {
    return Math.max(
      this.itemsGroup.maxRightXPosition,
      this.rolesElementsGroup.rightXPosition
    );
  }

  public set offset(offset: number) {
    this.rolesElementsGroup.offset = offset;

    this.itemsGroup.offsetFromRoleGroup = offset * 2;

    /*????*/
    const roleElements: Array<RoleElement> = this._calcRoleElements(this.rolesElementsGroup);
    const itemElements: Array<ItemElement> = this._calcItemElements(this.itemsGroup);

    this.itemToRoleArrowsGroup = new ItemToRoleArrowsGroup(
      itemElements,
      roleElements,
      this.relationship
    );

    this.roleToTargetArrowsGroup = new RoleToTargetArrowGroup(
      roleElements,
      this._targetElement,
      this.relationship
    );

    this.roleElements = roleElements;
    this.itemElements = itemElements;
  }

  protected _generateRelationElementsForGroup (
    rolesArray: Array<string>,
    roles: Array<RoleElemContent>
  ): Array<RoleElemContent> {
    return roles.filter((role: RoleElemContent) =>
      rolesArray.includes(role.role));
  }

  protected _calcRoleElements(
    rolesElementsGroup: RoleElementsGroup
  ): Array<RoleElement> {
    return rolesElementsGroup.roleElements;
  }

  protected _calcItemElements(
    itemsGroup: ItemsGroup
  ): Array<ItemElement> {
    return itemsGroup.itemsRoleGroups
      .map((itemsRoleGroup: ItemsRoleGroup): Array<ItemsLevelGroup> => (
        itemsRoleGroup.itemsLevelGroups
      ))
      .reduce(
        (
          accumulator: Array<ItemsLevelGroup>,
          itemsLevelGroup: Array<ItemsLevelGroup>
        ): Array<ItemsLevelGroup> => ([
          ...accumulator,
          ...itemsLevelGroup
        ])
      , []
      )
      .map((itemsLevelGroup: ItemsLevelGroup): Array<ItemElement> => (
        itemsLevelGroup.itemElements
      ))
      .reduce(
        (
          accumulator: Array<ItemElement>,
          itemsLevelGroup: Array<ItemElement>
        ): Array<ItemElement> => ([
          ...accumulator,
          ...itemsLevelGroup
        ])
      , []
      );
  }
}