import ItemElement from '../Items/ItemElement';
import { ProcessingItemElemContent } from '../../interfaces/elemsContent';

export default
class UniqueItems {
  public static getUniqueRolesInElementArr(
    elements: Array<ProcessingItemElemContent>
  ): Array<string> {
    return elements
      .map(({roleArr}: ProcessingItemElemContent): Array<string> => roleArr)
      .reduce((
        accumulator: Array<string>,
        roleArr: Array<string>
      ): Array<string> => ([
        ...accumulator,
        ...roleArr
      ]),     [])
      .filter((
        value: string,
        index: number,
        self: Array<string>
      ): boolean => (
        self.indexOf(value) === index
      ))
      .sort(
        (a, b) => {
          if (a > b) {
            return 1;
          } else if (a < b) {
            return -1;
          } else {
            return 0;
          }
        }
      );
  }

  public static getUniqRolesInLevelArr(itemElements: Array<ItemElement>): Array<string> {
    return itemElements
      .map(({roleArr}: ItemElement): Array<string> => (
        roleArr
      ))
      .reduce(
        (
          accumulator: Array<string>,
          curItem: Array<string>
        ): Array<string> => (
          [...accumulator, ...curItem]
        ),
        []
      )
      .filter((
        value: string,
        index: number,
        self: Array<string>
      ): boolean => (
        !!value && (self.indexOf(value) === index)
      ))
      .sort((a, b) => {
        if (a > b) {
          return 1;
        } else if (a < b) {
          return -1;
        } else {
          return 0;
        }
      });
  }
}