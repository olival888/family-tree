import RoleElement from '../Roles/RoleElement';
import ItemElement from '../Items/ItemElement';
import { INDENTS_BETWEEN_ELEMENTS } from '../../constants/indents';
import { ProcessingItemElemContent, RoleElemContent } from '../../interfaces/elemsContent';

interface Element {
  size: Size;
  centralCoords: Coords;
}

export default
class Elements {
  public static initRoleElement(
    contentElements: Array<RoleElemContent>,
    indentsBetweenElements: number
  ): Array<RoleElement> {
    const elements: Array<RoleElement> = contentElements.map(
      (
        contentElement: RoleElemContent
      ): RoleElement =>
        new RoleElement(contentElement)
    );

    return contentElements.length ? this._setXPosition<RoleElement>(elements, indentsBetweenElements) : [];
  }

  public static initItemElement(
    contentElements: Array<ProcessingItemElemContent>,
    last: boolean,
    indentInCenter: number,
    roleGroupOrderIndex: number,
    indentsBetweenElements: number
  ): Array<ItemElement> {
    const elements: Array<ItemElement> = contentElements
      .map((contentElement: ProcessingItemElemContent): ItemElement =>
        new ItemElement(
          contentElement
        )
      );

    const positionedElements: Array<ItemElement> =
      this._setXPosition<ItemElement>(elements, indentsBetweenElements, last);

    if (!last) {
      const centerIndex: number = Math.ceil(elements.length / 2);

      const leftElements: Array<ItemElement> = positionedElements.slice(0, centerIndex)
        .map((itemElement: ItemElement): ItemElement => {

          itemElement.centralCoords = {
            x: itemElement.centralCoords.x
            - indentInCenter / 2
            + roleGroupOrderIndex * INDENTS_BETWEEN_ELEMENTS / 2,
            y: itemElement.centralCoords.y
          };

          return itemElement;
        });

      const rightElements: Array<ItemElement> = positionedElements.slice(centerIndex)
        .map((itemElement: ItemElement): ItemElement => {
          itemElement.centralCoords = {
            x: itemElement.centralCoords.x + indentInCenter / 2,
            y: itemElement.centralCoords.y
          };

          return itemElement;
        });

      return [
        ...leftElements,
        ...rightElements
      ];
    }

    return positionedElements;
  }

  protected static _setXPosition<T extends Element>(
    elements: Array<T>,
    indentsBetweenElements: number,
    last: boolean | undefined = undefined
  ): Array<T> {
    const elementsWithPosition: Array<T> = elements.map(
      (
        element: T,
        index: number,
        elemsArr: Array<T>
      ): T => {
        const prevElement: T = elemsArr[index - 1];

        element.centralCoords = {
          x: (index > 0) ?
            prevElement.centralCoords.x +
            prevElement.size.width / 2 +
            indentsBetweenElements +
            element.size.width / 2 :
            // unknown target position
            0,
          y: element.centralCoords.y
        };

        return element;
      }
    );

    let elemCenterX: number;

    if (elementsWithPosition.length % 2 === 0) {
      elemCenterX = elementsWithPosition[elements.length / 2 - 1].centralCoords.x +
        elementsWithPosition[elements.length / 2 - 1].size.width / 2 +
        indentsBetweenElements / 2;
    } else {
      elemCenterX = elementsWithPosition[Math.floor(elements.length / 2)].centralCoords.x;
    }

    if (last === false) {
      elemCenterX = elementsWithPosition[Math.ceil(elements.length / 2) - 1].centralCoords.x +
        elementsWithPosition[Math.ceil(elements.length / 2) - 1].size.width / 2 +
        indentsBetweenElements / 2;
    }

    return elementsWithPosition.map((element: T): T => {
      element.centralCoords.x = element.centralCoords.x - elemCenterX;

      return element;
    });
  }
}