import {
  ItemElemContent,
  ProcessingItemElemContent,
  ProcessingTargetElemContent,
  TargetElemContent
} from '../../interfaces/elemsContent';

export default
class ProcessingElements {
  public static processingItemElements({
                                          id,
                                          regNo,
                                          title,
                                          type,
                                          roles,
                                          favorites,
                                          url_s,
                                          ...tooltipInfo
                                        }: ItemElemContent): ProcessingItemElemContent {
    return {
      id,
      regNo,
      title,
      type,
      roles,
      url_s,
      favorites,
      roleArr: Object.keys(roles),
      tooltipInfo
    };
  }

  public static processingTargetElements({
                                         id,
                                         regNo,
                                         title,
                                         type,
                                         favorites,
                                         url_s,
                                         ...tooltipInfo
                                       }: TargetElemContent): ProcessingTargetElemContent {
    return {
      id,
      regNo,
      title,
      type,
      url_s,
      favorites,
      tooltipInfo
    };
  }
}