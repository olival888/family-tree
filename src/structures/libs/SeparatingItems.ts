import ItemElement from '../Items/ItemElement';
import { ProcessingItemElemContent } from '../../interfaces/elemsContent';

export default
class SeparatingItems {
  public static separationOfElementsByRoles(
    elements: Array<ProcessingItemElemContent | ItemElement>,
    rolesArray: Array<string>
  ): Array<Array<ProcessingItemElemContent | ItemElement>> {
    return rolesArray
      .map((role: string, index: number): Array<ProcessingItemElemContent | ItemElement> => {
        return elements
          .filter((element: ProcessingItemElemContent | ItemElement): boolean =>
            element.roleArr[0] === role
          );
      })
      .filter((item: Array<ProcessingItemElemContent | ItemElement>): boolean => item.length > 0);
  }

  public static separationOfElementsByLevels<T>(
    elements: Array<T>,
    levelSize: number
  ) {
    return elements.reduce((
      accumulator: Array<Array<T>>,
      element: T
    ): Array<Array<T>> => {
      const length: number = accumulator.length;

      if (accumulator[length - 1].length < levelSize) {
        const lastLevel: Array<T> = accumulator[length - 1];

        accumulator[length - 1] = [...lastLevel, element];

        return accumulator;
      } else {
        accumulator[length] = [element];

        return accumulator;
      }
    },                     [[]]);
  }
}