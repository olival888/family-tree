import {
  ELEMENT_BLOCK_PADDING,
  ELEMENT_HEIGHT, ROLE_HEIGHT, TARGET_ELEMENT_HEIGHT
} from '../../constants/commonElementsStyleProps';

export default
class SizeItem {
  public static getSizeTargetElement(id: string, title: string): Size {
    return this.getSizeElement(
      {
        id, title
      },
      TARGET_ELEMENT_HEIGHT,
      ELEMENT_BLOCK_PADDING
    );
  }
  public static getSizeRoleElement(title: string): Size {
    return this.getSizeElement(
      {
        title
      },
      ROLE_HEIGHT,
      ELEMENT_BLOCK_PADDING
    );
  }
  public static getSizeItemElement(id: string, title: string): Size {
    return this.getSizeElement(
      {
        id, title
      },
      ELEMENT_HEIGHT,
      ELEMENT_BLOCK_PADDING
    );
  }

  protected static getSizeElement(
    content: {[contentField: string]: string},
    height: number,
    padding: number
  ): Size {
    const element: HTMLDivElement = document.createElement('div');
    element.setAttribute(
      'style',
      `font-family: Arial;
      display: inline-flex;
      flex-direction: column;
      justify-content: space-between;
      height: ${height}px;
      padding: ${padding}px;
      font-size: 16px;
      white-space: nowrap;
      visibility: hidden`
    );

    Object.entries(content).forEach(([key, value]: Array<string>, index: number): void => {
      const fieldElem: HTMLDivElement = document.createElement('div');

      if (key === 'id') {
        fieldElem.setAttribute(
          'style',
          `font-weight: bold;`
        );
      }

      fieldElem.innerText = value;

      element.appendChild(fieldElem);
    });

    document.body.appendChild(element);

    const size: ClientRect = element.getBoundingClientRect();

    document.body.removeChild(element);

    return {
      width: size.width,
      height: size.height
    };
  }
}