import RelationGroup from '../RelationGroup';
import Element from '../TargetElement';
import { INDENTS_BETWEEN_ELEMENTS, INDENTS_BETWEEN_LEVELS } from '../../constants/indents';
import ItemsLevelGroup from '../Items/ItemsLevelGroup';

export default
class Offsets {
  public static offsetBetweenRelationGroupAndTarget(
    relationGroup: RelationGroup,
    targetElement: Element
  ): number {
    const indentBetweenTargetAndRoles: number =
      Math.ceil((relationGroup.rolesElementsGroup.roleElements.length + 1) / 2)
      * (INDENTS_BETWEEN_ELEMENTS / 2);

    return targetElement.size.height / 2 +
      targetElement.centralCoords.y +
      indentBetweenTargetAndRoles;
  }

  public static offsetBetweenLevels(itemsLevelGroups: Array<ItemsLevelGroup>): Array<number> {
    const levelsHeightArray: Array<number> = itemsLevelGroups
      .map((itemsLevelGroup: ItemsLevelGroup): number =>
        itemsLevelGroup.height);

    const uniqRolesSizeArray: Array<number> = itemsLevelGroups
      .map((itemsLevelGroup: ItemsLevelGroup): number =>
        itemsLevelGroup.uniqRolesSize);

    const offsetArray: Array<number> = uniqRolesSizeArray
      .map((uniqRolesInLevelsSize: number): number =>
        (INDENTS_BETWEEN_LEVELS / 2) * (uniqRolesInLevelsSize + 1));

    return levelsHeightArray.reduce(
      (
        accumulator: Array<number>,
        height: number,
        i: number
      ): Array<number> => ([
        ...accumulator,
        (i > 0) ?
          accumulator[i - 1]
          + levelsHeightArray[i - 1]
          + offsetArray[i] :
          offsetArray[i]
      ])
      ,
      []
    );
  }
}