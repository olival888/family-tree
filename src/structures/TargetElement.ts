import Element from './Element';
import { ProcessingTargetElemContent } from '../interfaces/elemsContent';

export default
class TargetElement extends Element {
  constructor(
    content: ProcessingTargetElemContent
  ) {
    super(content);
  }

  public get topYPosition(): number {
    return this.size.height / 2;
  }

  public get leftXPosition(): number {
    return this.size.width / 2;
  }

  public get rightXPosition(): number {
    return this.size.width / 2;
  }
}