import RoleElement from './RoleElement';
import { CHILD } from '../../constants/relations';
import Elements from '../libs/Elements';
import { RoleElemContent } from '../../interfaces/elemsContent';

export default
class RoleElementsGroup {
  public roleElements: Array<RoleElement>;

  protected _relation: string;

  constructor(
    elements: Array<RoleElemContent>,
    relationship: string,
    indentsBetweenElements: number
  ) {
    this.roleElements = Elements.initRoleElement(elements, indentsBetweenElements);

    this._relation = relationship;
  }

  public set offset(offset: number) {
    const sign = this._relation === CHILD ? 1 : -1;

    this.roleElements.map((element: RoleElement): RoleElement => {
      element.centralCoords = {
        x: element.centralCoords.x,
        y: sign * (offset + element.size.height / 2)
      };

      return element;
    });
  }

  public get leftXPosition(): number {
    return this.roleElements.length ?
      Math.abs(this.roleElements[0].centralCoords.x) + this.roleElements[0].size.width / 2 :
      0;
  }

  public get rightXPosition(): number {
    return this.roleElements.length ?
      Math.abs(this.roleElements[this.roleElements.length - 1].centralCoords.x)
        + this.roleElements[this.roleElements.length - 1].size.width / 2 :
      0;
  }
}