import SizeItem from '../libs/SizeItem';
import { RoleElemContent } from '../../interfaces/elemsContent';

export default
class RoleElement {
  public readonly title: string;
  public readonly role: string;

  public readonly size: Size;

  public centralCoords: Coords;

  constructor(
    content: RoleElemContent
  ) {
    this.title = content.title;
    this.role = content.role;

    this.size = SizeItem.getSizeRoleElement(content.title);
    this.centralCoords = {x: 0, y: 0};
  }
}
