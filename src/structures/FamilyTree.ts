import TargetElement from './TargetElement';
import RelationGroup from './RelationGroup';

import Offsets from './libs/Offsets';

import { PARENT, CHILD } from '../constants/relations';
import {
  ItemElemContent,
  RoleElemContent,
  TargetElemContent
} from '../interfaces/elemsContent';
import ProcessingElements from './libs/ProcessingElements';

export default
class FamilyTree {
  public parentGroup: RelationGroup;
  public childGroup: RelationGroup;

  public targetElement: TargetElement;

  constructor(
    target: TargetElemContent,
    parents: Array<ItemElemContent>,
    children: Array<ItemElemContent>,
    roles: Array<RoleElemContent>,
    targetCentralCoord: Coords = {x: 0, y: 0}
  ) {
    this.targetElement = new TargetElement(ProcessingElements.processingTargetElements(target));

    this.targetElement.centralCoords = targetCentralCoord;

    this.parentGroup = new RelationGroup(
      PARENT,
      parents,
      roles,
      this.targetElement
    );

    this.childGroup = new RelationGroup(
      CHILD,
      children,
      roles,
      this.targetElement
    );

    this.parentGroup.offset = Offsets.offsetBetweenRelationGroupAndTarget(
      this.parentGroup,
      this.targetElement
    );

    this.childGroup.offset = Offsets.offsetBetweenRelationGroupAndTarget(
      this.childGroup,
      this.targetElement
    );
  }

  public get width(): number {
    /*return Math.max(
      this.parentGroup.maxLeftXPosition,
      this.childGroup.maxLeftXPosition
    ) + Math.max(
      this.parentGroup.maxRightXPosition,
      this.childGroup.maxRightXPosition
    );*/
    return Math.max(
      this.targetElement.leftXPosition,
      this.targetElement.rightXPosition,
      this.parentGroup.maxLeftXPosition,
      this.childGroup.maxLeftXPosition,
      this.parentGroup.maxRightXPosition,
      this.childGroup.maxRightXPosition
    ) * 2;
  }

  public get height(): number {
    // return this.parentGroup.topYPosition + this.childGroup.topYPosition;
    return Math.max(
      this.targetElement.topYPosition,
      this.parentGroup.topYPosition,
      this.childGroup.topYPosition
    ) * 2;
  }

  public get targetElementCentralPosition(): Coords {
    return {
      x: Math.max(
        this.targetElement.leftXPosition,
        this.parentGroup.maxLeftXPosition,
        this.childGroup.maxLeftXPosition
      ),
      y: Math.max(
        this.targetElement.topYPosition,
        this.parentGroup.topYPosition
      )
    };
  }
}
