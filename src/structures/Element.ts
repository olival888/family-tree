import SizeItem from './libs/SizeItem';
import { ProcessingElemContent, TooltipInfo } from '../interfaces/elemsContent';

export default
class Element {
  public readonly id: string;
  public readonly title: string;
  public readonly type: string;

  public readonly tooltipInfo: Partial<TooltipInfo>;

  public readonly size: Size;

  public centralCoords: Coords;

  constructor(
    content: ProcessingElemContent
  ) {

    this.id = content.id;
    this.title = content.title;
    this.type = content.type;
    this.tooltipInfo = content.tooltipInfo;

    this.centralCoords = {x: 0, y: 0};

    this.size = SizeItem.getSizeTargetElement(content.id, content.title);
  }
}