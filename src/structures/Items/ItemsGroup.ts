import ItemsRoleGroup from './ItemsRoleGroup';

import SeparatingItems from '../libs/SeparatingItems';
import { ProcessingItemElemContent } from '../../interfaces/elemsContent';

export default
class ItemsGroup {
  public itemsRoleGroups: Array<ItemsRoleGroup>;

  constructor(
    elements: Array<ProcessingItemElemContent>,
    rolesArray: Array<string>,
    relationship: string
  ) {
    const itemsRoleGroups: Array<ItemsRoleGroup> = SeparatingItems
      .separationOfElementsByRoles(elements, rolesArray)
      .map((
        itemsRoleGroup: Array<ProcessingItemElemContent>,
        index: number,
        array: Array<Array<ProcessingItemElemContent>>
      ): ItemsRoleGroup => {
        return new ItemsRoleGroup(
            itemsRoleGroup,
            relationship,
            rolesArray,
            index,
            index === (array.length - 1)
        );
      });

    // calc offset in lib
    this.itemsRoleGroups = itemsRoleGroups.reduce(
      (
        result: Array<ItemsRoleGroup>,
        itemsRoleGroup: ItemsRoleGroup,
        index: number
      ): Array<ItemsRoleGroup> => {
        const prevItemsRoleGroup: ItemsRoleGroup | null = (index > 0) ? result[index - 1] : null;

        itemsRoleGroup.offset = (prevItemsRoleGroup) ?
          prevItemsRoleGroup.offset + prevItemsRoleGroup.height :
          0;

        return [...result, itemsRoleGroup];
      }
      ,
      []
    );
  }

  public set offsetFromRoleGroup(offset: number) {
    this.itemsRoleGroups = this.itemsRoleGroups
      .map((itemsRoleGroup: ItemsRoleGroup): ItemsRoleGroup => {
        itemsRoleGroup.offset = offset;

        return itemsRoleGroup;
      });
  }

  public get topYPosition(): number {
    return this.itemsRoleGroups.length ?
      this.itemsRoleGroups[this.itemsRoleGroups.length - 1].topYPosition :
      0;
  }

  public get maxLeftXPosition(): number {
    return this.itemsRoleGroups.length ?
      Math.max(
        ...this.itemsRoleGroups.map(({maxLeftXPosition}: ItemsRoleGroup) => (maxLeftXPosition))
      ) :
      0;
  }

  public get maxRightXPosition(): number {
    return this.itemsRoleGroups.length ?
      Math.max(
        ...this.itemsRoleGroups.map(({maxRightXPosition}: ItemsRoleGroup) => (maxRightXPosition))
      ) :
      0;
  }
}
