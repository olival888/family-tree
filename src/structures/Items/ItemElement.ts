import Element from '../Element';
import { ProcessingItemElemContent, RoleInfo } from '../../interfaces/elemsContent';

export default
class ItemElement extends Element {
  public readonly roles: { [key: string]: RoleInfo };
  public readonly roleArr: Array<string>;

  constructor(
    content: ProcessingItemElemContent
  ) {
    super(content);

    this.roles = content.roles;
    this.roleArr = content.roleArr;
  }
}
