import ItemsLevelGroup from './ItemsLevelGroup';
import { ELEMENTS_IN_LEVEL } from '../../constants/other';

import SeparatingItems from '../libs/SeparatingItems';
import Offsets from '../libs/Offsets';
import { ProcessingItemElemContent } from '../../interfaces/elemsContent';

export default
class ItemsRoleGroup {
  public itemsLevelGroups: Array<ItemsLevelGroup>;

  public role: string;
  public height: number;

  protected _offset: number;

  constructor(
    elements: Array<ProcessingItemElemContent>,
    relationship: string,
    rolesArray: Array<string>,
    orderIndex: number,
    last: boolean
  ) {
    const itemsLevelGroups: Array<ItemsLevelGroup> = SeparatingItems.separationOfElementsByLevels(
      elements,
      ELEMENTS_IN_LEVEL
    )
      .map((
          level: Array<ProcessingItemElemContent>,
          index: number,
          array: Array<Array<ProcessingItemElemContent>>
        ): ItemsLevelGroup =>
          new ItemsLevelGroup(
            level,
            relationship,
            rolesArray,
            orderIndex,
            last && (index === (array.length - 1))
          )
      );

    const offsetBetweenLevels: Array<number> = Offsets.offsetBetweenLevels(itemsLevelGroups);

    this.itemsLevelGroups = itemsLevelGroups
      .map((itemsLevelGroup: ItemsLevelGroup, index: number): ItemsLevelGroup => {
        itemsLevelGroup.offsetBetweenLevels = offsetBetweenLevels[index];

        return itemsLevelGroup;
      });

    this.height = offsetBetweenLevels.slice(-1)[0] + this.itemsLevelGroups.slice(-1)[0].height;

    this.role = rolesArray[orderIndex];
  }

  public set offset(offset: number) {
    this._offset = offset;

    this.itemsLevelGroups
      .map((levelGroup: ItemsLevelGroup): ItemsLevelGroup => {
        levelGroup.offset = offset;

        return levelGroup;
      });
  }

  public get offset(): number {
    return this._offset;
  }

  public get topYPosition(): number {
    return this.itemsLevelGroups[this.itemsLevelGroups.length - 1].YPosition;
  }

  public get maxLeftXPosition(): number {
    return Math.max(
      ...this.itemsLevelGroups.map(({leftXPosition}: ItemsLevelGroup): number => Math.abs(leftXPosition))
    );
  }

  public get maxRightXPosition(): number {
    return Math.max(
      ...this.itemsLevelGroups.map(({rightXPosition}: ItemsLevelGroup): number => Math.abs(rightXPosition))
    );
  }
}
