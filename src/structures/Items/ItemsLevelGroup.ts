import ItemElement from './ItemElement';

import { INDENTS_BETWEEN_ELEMENTS } from '../../constants/indents';
import { CHILD } from '../../constants/relations';
import UniqueItems from '../libs/UniqueItems';
import Elements from '../libs/Elements';
import { ProcessingItemElemContent } from '../../interfaces/elemsContent';

export default
class ItemsLevelGroup {
  public itemElements: Array<ItemElement>;

  protected _relationship: string;

  constructor(
    elements: Array<ProcessingItemElemContent>,
    relationship: string,
    rolesArray: Array<string>,
    roleGroupOrderIndex: number,
    last: boolean
  ) {
    const indentInCenter: number = (rolesArray.length - 1) * (INDENTS_BETWEEN_ELEMENTS / 2);

    this.itemElements = Elements.initItemElement(
      elements,
      last,
      indentInCenter,
      roleGroupOrderIndex,
      INDENTS_BETWEEN_ELEMENTS
    );

    this._relationship = relationship;
  }

  public set offsetBetweenLevels(offset: number) {
    const sign: number = (this._relationship === CHILD) ? 1 : -1;

    this.itemElements.map((itemElement: ItemElement): ItemElement => {
      itemElement.centralCoords = {
        x: itemElement.centralCoords.x,
        y: itemElement.centralCoords.y
          + sign * (offset + itemElement.size.height / 2)
      };

      return itemElement;
    });
  }

  public set offset(offset: number) {
    const sign: number = (this._relationship === CHILD) ? 1 : -1;

    this.itemElements.map((itemElement: ItemElement, index: number): ItemElement => {
      itemElement.centralCoords = {
        x: itemElement.centralCoords.x,
        y: itemElement.centralCoords.y + sign * offset
      };

      return itemElement;
    });
  }

  public get height(): number {
    return Math.max(
      ...this.itemElements
        .map((itemElement: ItemElement): number =>
          itemElement.size.height)
    );
  }

  public get uniqRolesSize(): number {
    return UniqueItems.getUniqRolesInLevelArr(this.itemElements).length;
  }

  public get YPosition(): number {
    return Math.abs(this.itemElements[0].centralCoords.y) + this.itemElements[0].size.height / 2;
  }

  public get leftXPosition(): number {
    return Math.abs(this.itemElements[0].centralCoords.x) + this.itemElements[0].size.width / 2;
  }

  public get rightXPosition(): number {
    return Math.abs(this.itemElements[this.itemElements.length - 1].centralCoords.x) +
      this.itemElements[this.itemElements.length - 1].size.width / 2;
  }
}
