export const INDENTS_BETWEEN_ELEMENTS: number = 60;
export const INDENTS_BETWEEN_LEVELS: number = 40;