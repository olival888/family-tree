export const MEMBER_ROLE: '0' = '0';
export const GOVERNMENT_ROLE: '1' = '1';
export const CONVOCATION_ROLE: '2' = '2';
export const LIQUIDATOR_ROLE: '3' = '3';
export const PROCURATOR_ROLE: '4' = '4';
