export const ELEMENT_BLOCK_PADDING: number = 10;
export const ELEMENT_FONT_SIZE: number = 16;
export const TARGET_ELEMENT_HEIGHT: number = 60;
export const ELEMENT_HEIGHT: number = 45;
export const ROLE_HEIGHT: number = 30;